﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager game; //singleton
    public GameObject playerOne; // playerone gameobject for senses
    public GameObject playerTwo; //playertwo gameobject 
    public List<GameObject> enemyPrefabs; //hold all 4 enemies fabs for spawning
    public List<Transform> patrolPoints; //patrol points for enemies
    public List<Transform> manaPoints; //only for mana pickups
    public GameObject manaPickUp; 
    public List<Transform> playerSpawns; //hold all of the spawn points created by procedural, random pick for the player to spawn at
    public List<Transform> enemySpawns; //same as player spawns
    public List<Transform> powerUpSpawns; //all powerup spawn points, randomly select where powerups spawn in
    public List<GameObject> powerUpPickups; //hold all powerup prefabs
    public List<GameObject> activePowerUps; //hold active powerups, compare with maxPowerUps to see if more need spawned
    public List<Powerup> powerupScripts;
    public List<GameObject> activePlayers; //to test if both dead when multiplayer
    public Transform enemyParent; //get the empty game object transform to be enemies parent
    public Transform powerParent; //parent for powerups 
    public int maxPowerUps = 4;
    public float powerupSpawnRate;
    public float nextPowerUpSpawn;
    [Header("Bullet Life/Speed")]
    public float bulletSpeed;
    public float bulletLife;
    public int manaCost;
    [Header("Spawning Variables")]
    public bool spawnIn = false;
    public int maxEnemies = 4;
    [Header("Screen Variables")]
    public bool gameScreen = true;
    [Header("Game Variables")]
    public bool isMulti = false;
    public Camera p1Cam;
    public Camera p2Cam;
    public float sfxVolume;
    public float musicVolume;
    public int reward;
    [Header("Audio Sources")]
    public AudioSource gameMusic;
    public AudioSource sfxSource;
    [Header("Audio Clips")]
    public AudioClip button;
    public AudioClip pawnShot;
    public AudioClip pawnHit;
    public AudioClip pawnDeath;
    public AudioClip powerupGet;

    void Awake() //before start
    {
        if (game == null) //if game is null, make game this instance
        {
            game = this;
            DontDestroyOnLoad(gameObject);  //dont destroy on scene load
        }
        else
        {
            Destroy(gameObject); //destroy any secondary game instances
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gameScreen == true)
        {
            if (spawnIn == true) //if spawn in is true (makes so they spawn in after map and no error)
            {
                SpawnPlayer();
                SpawnEnemies();
                for (int i = 0; i < maxPowerUps; i++)
                {
                    SpawnPowers();
                    SpawnManas();
                }
                spawnIn = false; //set false
            }
        }
    }

    public void SpawnPlayer() //instantiate the player
    {
        if (isMulti == false)
        {
            int pSpawn = Random.Range(0, playerSpawns.Count); //random spawn
            GameObject player = Instantiate<GameObject>(playerOne,
                    playerSpawns[pSpawn].transform.position,
                    playerSpawns[pSpawn].transform.rotation);
            playerOne = player; //change GameObject variable
            p1Cam = player.GetComponentInChildren<Camera>();
            InputController p1control = player.GetComponent<InputController>(); //get the input controller
            p1control.controller = InputController.controls.wasd; //set control scheme
            activePlayers.Add(playerOne);
        }
        else
        {
            int pSpawnOne = Random.Range(0, playerSpawns.Count); //random spawn for 1
            int pSpawnTwo = Random.Range(0, playerSpawns.Count); //random spawn for 2
            //player one spawning
            GameObject p1 = Instantiate<GameObject>(playerOne,
                    playerSpawns[pSpawnOne].transform.position,
                    playerSpawns[pSpawnOne].transform.rotation);
            playerOne = p1; //change GameObject variable
            p1Cam = p1.GetComponentInChildren<Camera>(); //grab p1 camera for split screen
            //player two spawning
            GameObject p2 = Instantiate<GameObject>(playerTwo, playerSpawns[pSpawnTwo].transform.position, playerSpawns[pSpawnTwo].transform.rotation);
            playerTwo = p2;
            p2Cam = p2.GetComponentInChildren<Camera>(); //grab p2 cam for splitscreen
            ChangeToSplit(); //call split screen
            //change input controls for both
            InputController p1con = p1.GetComponent<InputController>(); //p1 input controller
            InputController p2con = p2.GetComponent<InputController>(); //p2 input controller
            p1con.controller = InputController.controls.wasd; //set to wasd
            p2con.controller = InputController.controls.arrows; //set to arrows
            activePlayers.Add(p1);
            activePlayers.Add(p2);
        }
    }

    public void SpawnEnemies() //instantiate the enemies
    {
        for (int i = 0; i < enemyPrefabs.Count; i++)
        {
            int eSpawn = Random.Range(0, enemySpawns.Count); //random spawn
            GameObject enemy = Instantiate<GameObject>(enemyPrefabs[i], enemySpawns[eSpawn].transform.position, enemySpawns[eSpawn].transform.rotation);
            enemy.GetComponent<Transform>().parent = enemyParent; //set parent for organization
        }
    }

    public void SpawnPowers() //instantiate the powers
    {
            int ppSpawn = Random.Range(0, powerUpSpawns.Count); //get a random spawn
            int ppItem = Random.Range(0, powerUpPickups.Count); //get a random powerup
            GameObject powerup = Instantiate<GameObject>(powerUpPickups[ppItem], powerUpSpawns[ppSpawn].transform.position, powerUpSpawns[ppSpawn].transform.rotation);
            powerup.GetComponent<Transform>().parent = powerParent;
            activePowerUps.Add(powerup); //put in activelist
    }

    public void SpawnManas()
    {
        int manaSpawn = Random.Range(0, manaPoints.Count);
        GameObject manaPick = Instantiate<GameObject>(manaPickUp, manaPoints[manaSpawn].transform.position, manaPoints[manaSpawn].transform.rotation);
        manaPick.GetComponent<Transform>().parent = powerParent;
    }

    public void ChangeToSplit()
    {
        p1Cam.rect = new Rect(0, 0, .5f, 1); //creating split screen, left cam p1
        p2Cam.rect = new Rect(.5f, 0, .5f, 1); //right cam p2
    }

    public void PlayClip(AudioClip clip, AudioSource source)
    {
        source.clip = clip;
        source.Play();
    }
}
