﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform tf;
    public PawnData shooter;
    public int bulletDamage = 5;

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); //get bullet objs transform
        Destroy(this.gameObject,GameManager.game.bulletLife); //destroy bullet once life is over
    }

    // Update is called once per frame
    void Update()
    {
        tf.Translate(Vector3.forward * GameManager.game.bulletSpeed * Time.deltaTime); //move the bullet forward
    }

    void OnTriggerEnter(Collider other)
    {
        GameManager.game.PlayClip(GameManager.game.pawnHit, GameManager.game.sfxSource); 
        Destroy(gameObject); //destroys bullet
        PawnData hit = other.GetComponent<PawnData>();
        if (hit != null) //if it hit a player or enemy
        {
            hit.health -= bulletDamage;
        }
        if (shooter != null && other.gameObject.CompareTag("Target"))
        {
            shooter.playerScore += GameManager.game.reward;
            shooter.ChangeScore();
        }
    }
}

