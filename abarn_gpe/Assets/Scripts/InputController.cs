﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public PawnData pawn;
    public enum controls { wasd, arrows };
    public controls controller;

    void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        pawn = GetComponent<PawnData>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = Vector3.zero; //start out not moving forward, assume not moving\

        if (controller == controls.wasd)
        {
            if (Input.GetKey(KeyCode.W))
            {
                if (pawn.noise != null)
                {
                    pawn.noise.currentVolume = Mathf.Max(pawn.noise.currentVolume,
                        pawn.moveNoise);
                }
                movement += Vector3.forward; //apply a forward movement to direction, positive
                pawn.pMover.MoveForward(movement); //pass to move forward
            }

            if (Input.GetKey(KeyCode.S))
            {
                if (pawn.noise != null)
                {
                    pawn.noise.currentVolume = Mathf.Max(pawn.noise.currentVolume,
                        pawn.moveNoise);
                }
                movement += -Vector3.forward;  //apply a backward movement to direction, negative
                pawn.pMover.MoveBackward(movement); //pass to move backward
            }

            if (Input.GetKey(KeyCode.A))
            {
                if (pawn.noise != null)
                {
                    pawn.noise.currentVolume = Mathf.Max(pawn.noise.currentVolume,
                        pawn.moveNoise);
                }
                pawn.pMover.Rotate(-pawn.rotateSpeed * Time.deltaTime); //send rotation to Rotate
            }

            if (Input.GetKey(KeyCode.D))
            {
                if (pawn.noise != null)
                {
                    pawn.noise.currentVolume = Mathf.Max(pawn.noise.currentVolume,
                        pawn.moveNoise);
                }
                pawn.pMover.Rotate(pawn.rotateSpeed * Time.deltaTime); //send rotation to Rotate
            }

            if (Input.GetKey(KeyCode.F) && Time.time > pawn.nextFire) //get input and check cooldown
            {
                if (pawn.mana > 0)
                {
                    GameManager.game.PlayClip(GameManager.game.pawnShot, GameManager.game.sfxSource);
                    pawn.nextFire = Time.time + pawn.fireRate; //update cooldown
                    GameObject bullet = Instantiate<GameObject>(pawn.bullet,
                        pawn.bulletSpawn.transform.position,
                        pawn.bulletSpawn.transform.rotation); //instantiate fireball/bullet
                    Bullet bData = bullet.GetComponent<Bullet>(); //get bullet's bullet script
                    if (bData != null)
                    {
                        bData.shooter = pawn;
                        pawn.ChangeMana();
                    }
                }
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                if (pawn.noise != null)
                {
                    pawn.noise.currentVolume = Mathf.Max(pawn.noise.currentVolume,
                        pawn.moveNoise);
                }
                movement += Vector3.forward; //apply a forward movement to direction, positive
                pawn.pMover.MoveForward(movement); //pass to move forward
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                if (pawn.noise != null)
                {
                    pawn.noise.currentVolume = Mathf.Max(pawn.noise.currentVolume,
                        pawn.moveNoise);
                }
                movement += -Vector3.forward;  //apply a backward movement to direction, negative
                pawn.pMover.MoveBackward(movement); //pass to move backward
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                if (pawn.noise != null)
                {
                    pawn.noise.currentVolume = Mathf.Max(pawn.noise.currentVolume,
                        pawn.moveNoise);
                }
                pawn.pMover.Rotate(-pawn.rotateSpeed * Time.deltaTime); //send rotation to Rotate
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                if (pawn.noise != null)
                {
                    pawn.noise.currentVolume = Mathf.Max(pawn.noise.currentVolume,
                        pawn.moveNoise);
                }
                pawn.pMover.Rotate(pawn.rotateSpeed * Time.deltaTime); //send rotation to Rotate
            }

            if (Input.GetKey(KeyCode.RightShift) && Time.time > pawn.nextFire) //get input and check cooldown
            {
                GameManager.game.PlayClip(GameManager.game.pawnShot, GameManager.game.sfxSource);
                pawn.nextFire = Time.time + pawn.fireRate; //update cooldown
                GameObject bullet = Instantiate<GameObject>(pawn.bullet,
                    pawn.bulletSpawn.transform.position,
                    pawn.bulletSpawn.transform.rotation); //instantiate fireball/bullet
                Bullet bData = bullet.GetComponent<Bullet>(); //get bullet's bullet script
                if (bData != null)
                {
                    bData.shooter = pawn;
                }
            }
        }
    }
}
