﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour
{
    public MeshRenderer[] childrenRenderers; //array for renderers so player goes stealthed in grass too

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) //hides the player from enemies, toggles on
        {
            childrenRenderers = GameManager.game.playerOne.GetComponentsInChildren<MeshRenderer>(); //update list
            foreach (MeshRenderer child in childrenRenderers) //for each through all
            {
                Color childColor = child.material.color; //get color
                childColor.a = 0.1f; //change alpha
                child.material.color = childColor; //reassign the color back to child
            }
            PawnData tempPlayer = GameManager.game.playerOne.GetComponent<PawnData>();
            tempPlayer.isVisible = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) //reveals player, toggle off
        {
            childrenRenderers = GameManager.game.playerOne.GetComponentsInChildren<MeshRenderer>(); //update list
            foreach (MeshRenderer child in childrenRenderers) //for each through all
            {
                Color childColor = child.material.color; //get color
                childColor.a = 1.0f; //change alpha
                child.material.color = childColor; //reassign the color back to child
            }
            PawnData tempPlayer = GameManager.game.playerOne.GetComponent<PawnData>();
            tempPlayer.isVisible = true;
        }
    }
}
