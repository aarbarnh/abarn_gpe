﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SavingScript : MonoBehaviour
{
    public SaveData data;

    public void OnLoad()
    {
        string jsonData = PlayerPrefs.GetString("SavedData");
        data = JsonUtility.FromJson<SaveData>(jsonData);
        MenuController.menu.sfx.value = data.sfxSave * 100;
        MenuController.menu.music.value = data.musicSave * 100;
        GameManager.game.sfxVolume = data.sfxSave; //reload sfx volume
        GameManager.game.musicVolume = data.musicSave; //reload music volume
    }
    public void OnSave()
    {
        data.sfxSave = GameManager.game.sfxVolume; //set data variable before save
        data.musicSave = GameManager.game.musicVolume; //set data before save
        string jsonData = JsonUtility.ToJson(data);
        PlayerPrefs.SetString("SavedData", jsonData);
        PlayerPrefs.Save();
    }
}

[System.Serializable]
public class SaveData
{
    public float sfxSave;
    public float musicSave;
}
