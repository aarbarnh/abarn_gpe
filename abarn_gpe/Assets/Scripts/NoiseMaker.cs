﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour
{
    public float currentVolume = 0; //initialize volume
    public float volumeDecay = 0.015f; //volume decay per frame, mess with if needed

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentVolume > 0)
        {
            currentVolume -= volumeDecay;
        }
    }
}
