﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    [Header("Room doors")]
    public GameObject northDoor;
    public GameObject southDoor;
    public GameObject eastDoor;
    public GameObject westDoor;
    [Header("Spawns")]
    public Transform playerSpawn;
    public Transform enemySpawnOne;
    public Transform enemySpawnTwo;
    public Transform powerSpawnOne;
    public Transform powerSpawnTwo;
    [Header("Patrol Points")]
    public Transform patrolPtOne;
    public Transform patrolPtTwo;
    [Header("Mana Points")]
    public Transform manaPtOne;
    public Transform manaPtTwo;

    void Start()
    {
        //add all points to proper lists for instantiation later
        GameManager.game.enemySpawns.Add(enemySpawnOne);
        GameManager.game.enemySpawns.Add(enemySpawnTwo);
        GameManager.game.playerSpawns.Add(playerSpawn);
        GameManager.game.powerUpSpawns.Add(powerSpawnTwo);
        GameManager.game.powerUpSpawns.Add(powerSpawnOne);
        GameManager.game.patrolPoints.Add(patrolPtOne);
        GameManager.game.patrolPoints.Add(patrolPtTwo);
        GameManager.game.manaPoints.Add(manaPtOne);
        GameManager.game.manaPoints.Add(manaPtTwo);
    }
}
