﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnMover : MonoBehaviour
{
    public CharacterController cControl; //grab character controller for mover
    public PawnData pawn;

    void Awake() //called before start, first thing
    {
        cControl = GetComponent<CharacterController>(); //grab the character controller on obj
    }
    // Start is called before the first frame update
    void Start()
    {
        pawn = GetComponent<PawnData>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MoveForward(Vector3 direction) //move forward
    {
        Vector3 trueForward = pawn.tf.TransformDirection(direction); //take the direction and find the objs forward
        cControl.SimpleMove(trueForward * pawn.forwardSpeed); //use simplemove, applies gravity on its own
    }

    public void MoveBackward(Vector3 direction) //move backward, uses a different speed than forward
    {
        Vector3 trueBackward = pawn.tf.TransformDirection(direction); //take direction passed and find the objs backward
        cControl.SimpleMove(trueBackward * pawn.backwardSpeed); //move the obj, gravity applied
    }

    public void Rotate(float rotation)
    {
        pawn.tf.Rotate(new Vector3(0,rotation * pawn.rotateSpeed * Time.deltaTime,0)); //rotate along the y axis based on rotation passed in
    }

    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vToTarget = target - pawn.tf.position; //find the vector that represents the enemy to the patrol point
        Quaternion targetRotation = Quaternion.LookRotation(vToTarget); //find the quaternion that looks down to that vector, that point

        if (pawn.tf.rotation == targetRotation)
        {
            return false; //looking at the waypoint, no rotation needed
        }

        pawn.tf.rotation = Quaternion.RotateTowards(pawn.tf.rotation,
            targetRotation, pawn.rotateSpeed * Time.deltaTime); //rotate towards the rotate direction a bit at a time, not all at once
        return true;
    }
}
