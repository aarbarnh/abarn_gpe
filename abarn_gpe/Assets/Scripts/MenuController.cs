﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public LevelCreator lc;
    public static MenuController menu;

    [Header("Screen Objects")]
    public GameObject gameScreen;
    public GameObject optionScreen;
    public GameObject gameOverScreen;
    public GameObject mainMenuScreen;
    [Header("Toggles")]
    public Toggle lod;
    public Toggle multi;
    [Header("Sliders")]
    public Slider sfx;
    public Slider music;
    [Header("Text Objects")]
    public Text gameOverScore;

    private void Awake()
    {
        if (menu == null) //if game is null, make game this instance
        {
            menu = this;
            DontDestroyOnLoad(gameObject);  //dont destroy on scene load
        }
        else
        {
            Destroy(gameObject); //destroy any secondary game instances
        }
    }

    void Update()
    {
        RandomToggle();
        MultiToggle();
    }

    public void QuitGame()
    {
        GameManager.game.PlayClip(GameManager.game.button, GameManager.game.sfxSource);
        Application.Quit();
    }

    public void StartGame() //start the game
    {
        GameManager.game.PlayClip(GameManager.game.button,GameManager.game.sfxSource);
        mainMenuScreen.SetActive(false);
        gameScreen.SetActive(true);
        GameManager.game.gameScreen = true;
        GameManager.game.gameMusic.Play(); //start main music
        //shouldnt need game screen bool anymore, just call LevelBuild and put the game manager spawning functions in it
    }

    public void MainMenuReturn() //for main menu button
    {
        GameManager.game.PlayClip(GameManager.game.button, GameManager.game.sfxSource);
        optionScreen.SetActive(false);
        mainMenuScreen.SetActive(true);
    }

    public void Options() //for options button
    {
        GameManager.game.PlayClip(GameManager.game.button, GameManager.game.sfxSource);
        mainMenuScreen.SetActive(false);
        optionScreen.SetActive(true);
    }

    public void GameOver() //changing to game over once lives gone
    {
        GameManager.game.gameMusic.Stop(); //stop main music
        gameScreen.SetActive(false);
        GameManager.game.gameScreen = false;
        lc.DestroyWorld();
        gameOverScreen.SetActive(true);
        if (GameManager.game.isMulti == true)
        {
            int p1score = GameManager.game.playerOne.GetComponent<PawnData>().playerScore;
            int p2score = GameManager.game.playerTwo.GetComponent<PawnData>().playerScore;
            int totalScore = p1score + p2score;
            gameOverScore.text = "Score : " + totalScore;
        }
        else
        {
            gameOverScore.text = "Score : " + GameManager.game.playerOne.GetComponent<PawnData>().playerScore;
        }
    }

    public void RandomToggle() //change between random or lod depending on toggle
    {
        if (lod.isOn == false)
        {
            lc.isLOD = false;
            lod.isOn = false;
        }
        if (lod.isOn == true)
        {
            lc.isLOD = true;
            lod.isOn = true;
        }
    }

    public void MultiToggle() //change multiplayer if toggle is on or off
    {
        if (multi.isOn == true)
        {
            GameManager.game.isMulti = true;
        }
        if (multi.isOn == false)
        {
            GameManager.game.isMulti = false;
        }
    }

    public void SFXChange() //change sfx volume from slider
    {
        GameManager.game.sfxVolume = sfx.value / 100; //need to be between 0-1
        GameManager.game.sfxSource.volume = GameManager.game.sfxVolume;
    }

    public void MusicChange() //change music volume from slider
    {
        GameManager.game.musicVolume = music.value / 100; //change music volume based on slider
        GameManager.game.gameMusic.volume = GameManager.game.musicVolume; //reassign audio source volume with variable
    }
}
