﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PawnData : MonoBehaviour //all data needed by mover, input
{
    [Header("Components")] //shows in editor, separater/organize
    public PawnMover pMover; //reference to pawn mover
    public Transform tf; //reference to object transform
    public AISenses sense; //reference to AI sense
    public BehaviorFSM behavior; //reference to AI behaviors
    public NoiseMaker noise;
    public Health healthPts;

    [Header("Variables")]
    public float forwardSpeed; //player speed forward
    public float backwardSpeed; //player speed backward
    public float rotateSpeed; //player rotate speed
    public float moveNoise = 25.0f;
    public bool isVisible; //for grass patches (extra)
    public int playerScore = 0; //score, accumulates from destroys
    public int lives = 3; //player lives
    public int health = 50; //player health
    public int maxHealth = 50;
    public int mana = 100;
    public int maxMana = 100;
    public bool isEnemy;
    [Header("Bullet Information")]
    public GameObject bullet;
    public GameObject bulletSpawn;
    public float nextFire;
    public float fireRate;
    [Header("UI Elements")]
    public Text scoreText;
    public Text livesText;
    public Text manaText;

    void Awake() //before start, first thing to run
    {
        pMover = GetComponent<PawnMover>(); //grab the pawn mover script component
        tf = GetComponent<Transform>(); //grab the transform component 
        noise = GetComponent<NoiseMaker>(); //grab player noisemaker
        sense = GetComponent<AISenses>();
        behavior = GetComponent<BehaviorFSM>();
        healthPts = GetComponent<Health>(); //get player/enemies health
    }
// Start is called before the first frame update
    void Start()
    {
        if (scoreText != null && livesText != null)
        {
            scoreText.text = "Score : " + playerScore;
            livesText.text = "Lives : " + lives;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isEnemy == true)
        {
            if (health <= 0)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            if (health <= 0)
            {
                if (lives <= 0)
                {
                    GameManager.game.activePlayers.Remove(gameObject);
                    GetComponent<InputController>().enabled = false; //disable movement
                                                                     //gameOverText.enabled = true;
                    if (GameManager.game.activePlayers.Count <= 0)
                    {
                        MenuController.menu.GameOver();
                    }
                }
                else
                {
                    health = maxHealth; //restore health
                    lives -= 1; //decrement lives
                    ChangeLives();
                }
            }
        }
    }

    public void ChangeScore()
    {
        scoreText.text = "Score : " + playerScore;
    }

    public void ChangeLives()
    {
        livesText.text = "Lives : " + lives;
    }

    public void ChangeMana()
    {
        mana -= GameManager.game.manaCost;
        if (isEnemy == false)
        {
            manaText.text = "Mana : " + mana;
        }
    }
}
