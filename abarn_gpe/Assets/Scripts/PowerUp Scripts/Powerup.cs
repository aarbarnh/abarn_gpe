﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup : MonoBehaviour
{
    public float timeLeft; //time left for powerup to expire

    //virtual functions, to be overridden in children
    public virtual void OnApply(GameObject target)
    {
    }

    public virtual void OnRemove(GameObject target)
    {
    }
}
