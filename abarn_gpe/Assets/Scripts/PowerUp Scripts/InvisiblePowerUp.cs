﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisiblePowerUp : Powerup
{
    public float invisibleDuration;
    public MeshRenderer[] childrenRenderers;

    public override void OnApply(GameObject target)
    {
        timeLeft = invisibleDuration;
        PawnData pd = target.GetComponent<PawnData>();
        pd.isVisible = false; //turn invisible

        childrenRenderers = target.GetComponentsInChildren<MeshRenderer>(); //get renderers of all children
        foreach (MeshRenderer child in childrenRenderers) //for each through all
        {
            Color childColor = child.material.color; //get color
            childColor.a = 0.1f; //change alpha
            child.material.color = childColor; //reassign the color back to child
        } 
    }

    public override void OnRemove(GameObject target)
    {
        PawnData pd = target.GetComponent<PawnData>();
        pd.isVisible = true; //turn visible

        childrenRenderers = target.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer child in childrenRenderers)
        {
            Color childColor = child.material.color;
            childColor.a = 1.0f;
            child.material.color = childColor; //reassign the color back to child
        }
    }
}
