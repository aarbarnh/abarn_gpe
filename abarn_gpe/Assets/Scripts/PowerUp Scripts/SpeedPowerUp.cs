﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerUp : Powerup
{
    public float speedGain;
    public float speedDuration;

    public override void OnApply(GameObject target)
    {
        timeLeft = speedDuration; 
        PawnData pd = target.GetComponent<PawnData>();
        pd.forwardSpeed += speedGain; //add speed gain
    }

    public override void OnRemove(GameObject target)
    {
        PawnData pd = target.GetComponent<PawnData>();
        pd.forwardSpeed -= speedGain; //decrement speed gain
    }
}
