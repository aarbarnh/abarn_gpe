﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPowerUp : Powerup
{
    public int healthGain; //determine in inspector

    public override void OnApply(GameObject target) //override parent function for apply
    {
        PawnData hp = target.GetComponent<PawnData>(); //need to make everything grab from pawn data now
        if (hp.health == hp.maxHealth)
        {
            hp.health = hp.maxHealth;
        }
        else
        {
            hp.health += healthGain; //add amount to health
            if (hp.health > hp.maxHealth)
            {
                hp.health = hp.maxHealth;
            }
        }
    }

    public override void OnRemove(GameObject target)
    {
        //do nothing, do not want to remove health
    }
}
