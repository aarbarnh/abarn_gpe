﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisiblePickup : MonoBehaviour
{
    public Powerup invisible;
    // Start is called before the first frame update
    void Start()
    {
        invisible = GameManager.game.powerupScripts[1];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other) //applying powerup
    {
        PowerupController pCon = other.GetComponent<PowerupController>();
        if (pCon != null)
        {
            GameManager.game.PlayClip(GameManager.game.powerupGet, GameManager.game.sfxSource);
            pCon.Apply(invisible);
            Destroy(gameObject);
        }
    }
}
