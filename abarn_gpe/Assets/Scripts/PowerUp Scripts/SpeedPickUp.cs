﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPickUp : MonoBehaviour
{
    public Powerup speedUp;
    // Start is called before the first frame update
    void Start()
    {
        speedUp = GameManager.game.powerupScripts[2];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other) //applying
    {
        PowerupController pCon = other.GetComponent<PowerupController>();
        if (pCon != null)
        {
            GameManager.game.PlayClip(GameManager.game.powerupGet, GameManager.game.sfxSource);
            pCon.Apply(speedUp);
            Destroy(gameObject);
        }
    }
}
