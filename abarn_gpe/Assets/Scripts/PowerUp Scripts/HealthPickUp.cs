﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUp : MonoBehaviour
{
    public Powerup healthUp; //inspector visible public object
    // Start is called before the first frame update
    void Start()
    {
        healthUp = GameManager.game.powerupScripts[0];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other) //when player hits the pickup
    {
        PowerupController pCon = other.GetComponent<PowerupController>(); //get powerup controller from object (if powerup)
        if (pCon != null) //if it is a powerup, will have controller
        {
            GameManager.game.PlayClip(GameManager.game.powerupGet, GameManager.game.sfxSource);
            pCon.Apply(healthUp); //apply powerup
            Destroy(gameObject); //destroy after pickup
        }
    }
}
