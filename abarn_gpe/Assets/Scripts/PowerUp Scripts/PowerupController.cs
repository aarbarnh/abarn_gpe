﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    public List<Powerup> powerList; //list of active powerups

    // Start is called before the first frame update
    void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {
        PowerupTimerUpdate(); //always check for powerups to update timers
    }

    public void PowerupTimerUpdate()
    {
        List<Powerup> removalList = new List<Powerup>(); //create a secondary list to store powerups to be expired

        foreach (Powerup temp in powerList) //go through all powerList elements
        {
            temp.timeLeft -= Time.deltaTime; //count down
            if (temp.timeLeft <= 0)
            {
                removalList.Add(temp); //add powerup to removal list
            }
        }
        //go through removal list and remove
        foreach (Powerup remove in removalList)
        {
            Remove(remove);
        }
    }

    public void Remove(Powerup pUp)
    {
        pUp.OnRemove(gameObject); //call on remove for the powerups
        powerList.Remove(pUp); //remove powerup from active list
    }

    public void Apply(Powerup pUp)
    {
        powerList.Add(pUp); //add to active list
        pUp.OnApply(gameObject); //apply effect to gameobject
    }
    
}
