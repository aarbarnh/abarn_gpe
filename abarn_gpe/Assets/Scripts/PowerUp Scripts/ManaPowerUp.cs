﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPowerUp : Powerup
{
    public int manaGain = 20;

    public override void OnApply(GameObject target)
    {
        PawnData temp = target.GetComponent<PawnData>();
        if (temp.mana == temp.maxMana)
        {
            temp.mana = temp.maxMana;
            temp.manaText.text = "Mana : " + temp.mana;
        }
        else
        {
            temp.mana += manaGain;
            if (temp.mana > temp.maxMana)
            {
                temp.mana = temp.maxMana;
            }
            temp.manaText.text = "Mana : " + temp.mana;
        }
    }

    public override void OnRemove(GameObject target)
    {
        //nada
    }
}
