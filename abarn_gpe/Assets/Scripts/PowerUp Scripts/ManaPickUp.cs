﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPickUp : MonoBehaviour
{
    public Powerup manaUp;

    // Start is called before the first frame update
    void Start()
    {
        manaUp = GameManager.game.powerupScripts[3];
    }

    private void OnTriggerEnter(Collider other)
    {
        PowerupController pCon = other.GetComponent<PowerupController>(); //get powerup controller from object (if powerup)
        if (pCon != null) //if it is a powerup, will have controller
        {
            GameManager.game.PlayClip(GameManager.game.powerupGet, GameManager.game.sfxSource);
            pCon.Apply(manaUp); //apply powerup
            Destroy(gameObject); //destroy after pickup
        }
    }
}