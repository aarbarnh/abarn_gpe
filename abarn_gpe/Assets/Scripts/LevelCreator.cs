﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCreator : MonoBehaviour
{
    public List<GameObject> rooms; //list of rooms, 3 of them
    public GameObject[,] grid; //multidimensional array with all randomly selected rooms, thinking 3x3

    public int columns; //num of columns, 3
    public int rows; //num of rows, 3

    public float tWidth = 50.0f; //put y and x to 50 so should be offset of 50,50
    public float tHeight = 50.0f;

    [Header("Map Attributes")]
    public bool isLOD; //level of day bool
    public long longLOD; //long value for the level of day
    public int lodSeed; //seed, int, for level of day

    // Start is called before the first frame update
    void Start()
    {
        longLOD = System.DateTime.Today.ToBinary(); //get day as a binary and if lod is toggled, map of day
        lodSeed = (int)longLOD; //turn long to int to use
        LevelBuilder();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LevelBuilder()
    {
        if (isLOD)
        {
            Random.InitState(lodSeed); //level of the day seed
        }
        grid = new GameObject[columns,rows]; //create a grid with the column, row numbers settable in inspector
        for (int col = 0; col < columns; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                GameObject tRoom = Instantiate(RandRoom()); //instantiate a new room from random function
                grid[col, row] = tRoom; //put room into current index
                tRoom.transform.position = new Vector3(col * tWidth, 0, -row * tHeight);
                //set name later
                tRoom.GetComponent<Transform>().parent = gameObject.GetComponent<Transform>(); //set the parent to the level builder, more organized
                Room rScript = tRoom.GetComponent<Room>(); //grabbed for next door opening steps
            }
        }
        OpenDoors();
        GameManager.game.spawnIn = true;
    }

    GameObject RandRoom() //get a random tile from array
    {
        int room = Random.Range(0, rooms.Count);
        return rooms[room];
    }

    private void OpenDoors()
    {
        for (int col = 0; col < columns; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                Room rScript = grid[col, row].GetComponent<Room>();
                if (col != 0) //if it is not on the left end, if is keep door closed
                {
                    rScript.westDoor.SetActive(false);
                }
                if (col != columns - 1) //if it is not on right side, if it is keep door closed
                {
                    rScript.eastDoor.SetActive(false);
                }
                if (row != 0)
                {
                    rScript.northDoor.SetActive(false);
                }
                if (row != rows - 1)
                {
                    rScript.southDoor.SetActive(false);
                }
            }
        }
    }

    public void DestroyWorld()
    {
        for (int col = 0; col < columns; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                Destroy(grid[col, row]);
            }
        }
    }
}
