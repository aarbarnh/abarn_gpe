﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public PawnData pawn;
    public int currentHealth = 50;
    public int maxHealth = 50;

    // Start is called before the first frame update
    void Start()
    {
        pawn = GetComponent<PawnData>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DealDamage(int damage, PawnData original)
    {
        currentHealth -= damage; //take health away 
        if (currentHealth <= 0) //if player/ enemy dies
        {
            if (original != null) //if there is a pawn data, its a player/ enemy
            {
                original.playerScore += GameManager.game.reward; //add reward to score
            }
            Destroy(gameObject); //destroys player/enemy
        }
    }
}
