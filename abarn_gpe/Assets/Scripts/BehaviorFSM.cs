﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorFSM : MonoBehaviour
{
    public enum behaviorList //four behavior modes
    {
        Pete,
        Winston,
        Larry,
        Sam
    };

    public behaviorList bList; //variable to hold and set each AI
}
