﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISenses : MonoBehaviour
{
    [Header("AI Sense Variables")]
    public float viewDistance = 5.0f; //how far AI can see
    public float fov = 60.0f; //field of view
    public float hearDistance = 2.0f; //hearing distance
    const float DEGREES_TO_RADIANS = Mathf.PI / 180.0f;
    const float DEBUG_ANGLE_DISTANCE = 2.0f; //might not need, if greyed at end delete

    public PawnData enemy;
    public PawnData player; //for isVisible

    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponent<PawnData>();
        player = GameManager.game.playerOne.GetComponent<PawnData>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Hearing(GameObject target)
    {
        NoiseMaker tNoise = target.GetComponent<NoiseMaker>(); //get the noisemaker from player
        if (tNoise == null) //if no noise, can't hear
        {
            return false;
        }

        Transform targetPos = target.GetComponent<Transform>(); //get players transform for distance 
        if (Vector3.Distance(targetPos.position, enemy.tf.position) <=
            tNoise.currentVolume * hearDistance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool Seeing(GameObject target) //seeing the player
    {
        Collider tCollider = target.GetComponent<Collider>();
            if (tCollider == null)
            {
                return false;
            }

            Transform tPos = target.GetComponent<Transform>();
            Vector3 vToTarget = tPos.position - enemy.tf.position;
            vToTarget.Normalize();

            if (Vector3.Angle(vToTarget, enemy.tf.forward) >= fov)
            {
                return false;
            }

            RaycastHit hit;
            if (Physics.Raycast(enemy.tf.position, enemy.tf.forward, out hit,
                viewDistance))
            {
                if (hit.collider == null)
                {
                    return false;
                }

                if (hit.collider.CompareTag("Player") && player.isVisible)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
    }
}
