﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class AIController : MonoBehaviour
{
    private int avoidStage = 0; //state of avoid, 0 is none, 1 is turn, 2 is move
    public PawnData enemy; //grab enemy data for multiple enemies to use same variable name
    public PawnData player; //just to grab isVisible from player
    public float avoidExitTime; //for moving after turning in avoidance, set to 2
    public float stateEnterTime; //entering state times
    public int currentPoint = 0;
    public float closeEnough = 1.0f;
    public float larryGiveUp; //specific for larry, if player too far
    public bool isForward;
    public enum patrolModes //patrol enum
    {
        Loop,
        PingPong,
        Random
    };

    public enum actions //AI actions enum
    {
        Idle,
        Look,
        Patrol,
        Chase,
        Flee,
    };

    public actions aMode;
    public patrolModes pMode;

    [Header("Sam Variables")]
    public float fireRange;

    [Header("Winston Variables")]
    public Transform homePos;

    // Start is called before the first frame update
    private void Awake()
    {
        enemy = GetComponent<PawnData>(); //grab data to have access to all else
        player = GameManager.game.playerOne.GetComponent<PawnData>(); //sololy for player's data, hopefully it works or will have to revert
        currentPoint = Random.Range(0, GameManager.game.patrolPoints.Count); //set current patrol point to random
        homePos = enemy.tf; //used for winstons home pos
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (enemy.behavior.bList == BehaviorFSM.behaviorList.Larry) //Larry behavior FSM
        {
            switch (aMode)
            {
                case actions.Idle: //larry is idling, sleeping
                    Idle();
                    if (enemy.sense.Hearing(GameManager.game.playerOne))
                    {
                        if (player.isVisible)
                        {
                            ChangeAction(actions.Chase);
                        }
                    }
                    break;
                case actions.Chase: //larry is chasing, not for long
                    if (avoidStage != 0)
                    {
                        ObstacleAvoidance();
                    }
                    else
                    {
                        Chase(player.tf.position);
                    }
                    if (Vector3.Distance(enemy.tf.position,player.tf.position) > larryGiveUp || !enemy.sense.Hearing(GameManager.game.playerOne))
                    {
                        ChangeAction(actions.Idle);
                    }
                    if (!player.isVisible)
                    {
                        ChangeAction(actions.Idle);
                    }
                    break;
            }
        }
        else if (enemy.behavior.bList == BehaviorFSM.behaviorList.Sam) //sam behavior FSM
        {
            switch (aMode)
            {
                case actions.Idle: //if sam cannot see you (only in grass patches)
                    Idle();
                    if (player.isVisible && Time.time > stateEnterTime + 5)
                    {
                        ChangeAction(actions.Chase); //if player becomes visible, go back to chase
                    }
                    break;
                case actions.Chase: //majority of sam's time
                    if (avoidStage != 0)
                    {
                        ObstacleAvoidance();
                    }
                    else
                    {
                        Chase(player.tf.position);
                    }
                    if (Vector3.Distance(player.tf.position,enemy.tf.position) <= fireRange)
                    {
                        if (Time.time > enemy.nextFire)
                        {
                            Shoot();
                        }
                    }
                    if (!player.isVisible)
                    {
                        ChangeAction(actions.Idle);
                    }
                    if (Time.time > stateEnterTime + 8)
                    {
                        ChangeAction(actions.Idle);
                    }
                    break;
            }
        }
        else if (enemy.behavior.bList == BehaviorFSM.behaviorList.Pete) //pete behavior FSM
        {
            switch (aMode)
            {
                case actions.Patrol:
                    if (avoidStage != 0)
                    {
                        ObstacleAvoidance();
                    }
                    else
                    {
                        Patrol();
                    }
                    if (enemy.sense.Hearing(GameManager.game.playerOne))
                    {
                        if (player.isVisible)
                        {
                            ChangeAction(actions.Chase);
                        }
                    }
                    break;
                case actions.Chase:
                    if (avoidStage != 0)
                    {
                        ObstacleAvoidance();
                    }
                    else
                    {
                        Chase(player.tf.position);
                    }

                    if (Time.time > enemy.nextFire)
                    {
                        Shoot();
                    }
                    if (!enemy.sense.Seeing(GameManager.game.playerOne) || !enemy.sense.Hearing(GameManager.game.playerOne))
                    {
                        ChangeAction(actions.Patrol);
                    }

                    if (!player.isVisible)
                    {
                        ChangeAction(actions.Patrol);
                    }
                    break;
            }
        }
        else if (enemy.behavior.bList == BehaviorFSM.behaviorList.Winston) //winston behavior FSM 
        {
            switch (aMode)
            {
                case actions.Look:
                    Look();
                    if (enemy.sense.Seeing(GameManager.game.playerOne))
                    {
                        if (player.isVisible)
                        {
                            ChangeAction(actions.Chase);
                        }
                    }
                    break;
                case actions.Chase:
                    if (avoidStage != 0)
                    {
                        ObstacleAvoidance();
                    }
                    else
                    {
                        Chase(player.tf.position);
                    }

                    if (Time.time > enemy.nextFire)
                    {
                        Shoot();
                    }
                    if (Vector3.Distance(enemy.tf.position, player.tf.position) <=
                        closeEnough || !player.isVisible)
                    {
                        ChangeAction(actions.Flee);
                    }
                    break;
                case actions.Flee:
                    if (avoidStage != 0)
                    {
                        ObstacleAvoidance();
                    }
                    else
                    {
                        ReturnHome();
                    }

                    if (Vector3.Distance(homePos.position,enemy.tf.position) <= closeEnough)
                    {
                        ChangeAction(actions.Look);
                    }
                    break;
            }
        }
    }

    public void Shoot() //instantiate shot, update next shot
    {
        enemy.nextFire = Time.time + enemy.fireRate;
        GameObject bullet = Instantiate<GameObject>(enemy.bullet,
            enemy.bulletSpawn.transform.position,
            enemy.bulletSpawn.transform.rotation);
    }

    public void Patrol()
    {
        if (enemy.pMover.RotateTowards(GameManager.game.patrolPoints[currentPoint].position,
            enemy.rotateSpeed)) //if rotatetowards returns back that not facing, continue to rotate, don't move
        {
            //nada
        }
        else
        {
            if (CanMove())
            {
                enemy.pMover.cControl.SimpleMove(
                    enemy.tf.forward * enemy.forwardSpeed); //move towards way point
            }
            else
            {
                avoidStage = 1;
            }
        }

        if (Vector3.Distance(enemy.tf.position, GameManager.game.patrolPoints[currentPoint].position) <
            closeEnough)
        {
            if (isForward)
            {
                currentPoint++; //move on to next point in the list
            }
            else
            {
                currentPoint--;
            }

            if (currentPoint >= GameManager.game.patrolPoints.Count || currentPoint < 0)
            {
                if (pMode == patrolModes.Loop)
                {
                    currentPoint = 0;
                }
                else if (pMode == patrolModes.Random)
                {
                    currentPoint = Random.Range(0, GameManager.game.patrolPoints.Count);
                }
                else if (pMode == patrolModes.PingPong)
                {
                    isForward = !isForward;
                    if (currentPoint >= GameManager.game.patrolPoints.Count)
                    {
                        currentPoint = GameManager.game.patrolPoints.Count - 1;
                    }
                    else
                    {
                        currentPoint = 0;
                    }
                }
            }
        }
    }

    public void ChangeAction(actions newAction)
    {
        aMode = newAction; //set new action of fsm
        stateEnterTime = Time.time; //state start times, used for sam
    }

    public void Idle()
    {
        //nothing
    }

    public void Look()
    {
        enemy.pMover.Rotate(1); //rotate to simulate looking
    }

    public void Chase(Vector3 target)
    {
        Vector3 vToTarget = target - enemy.tf.position; //get vector to target (player)
        Vector3.Normalize(vToTarget); //normalize
        enemy.pMover.RotateTowards(vToTarget, enemy.rotateSpeed); //rotate to player
        if (Vector3.Distance(target, enemy.tf.position) > closeEnough && !enemy.pMover.RotateTowards(vToTarget, enemy.rotateSpeed))
        {
            if (CanMove()) //check if anything in way
            {
                enemy.pMover.cControl.SimpleMove(
                    enemy.tf.forward * enemy.forwardSpeed); //chase player
            }
            else
            {
                avoidStage = 1; //set to 1, turn to avoid
            }
        }
    }

    public void ReturnHome()
    {
        enemy.pMover.RotateTowards(homePos.position, enemy.rotateSpeed); //rotate to home pos
        if (!enemy.pMover.RotateTowards(homePos.position, enemy.rotateSpeed))
        {
            if (CanMove()) //check if anything in way
            {
                enemy.pMover.cControl.SimpleMove(
                    enemy.tf.forward * enemy.forwardSpeed); //move to home pos (fleeing)
            }
            else
            {
                avoidStage = 1; //set to 1, turn to avoid
            }
        }
    }

    public void ObstacleAvoidance()
    {
        if (avoidStage == 1)
        {
            enemy.pMover.Rotate(-1);
            if (CanMove())
            {
                avoidStage = 2; //move to stage 2, moving
                avoidExitTime = 2.0f;
            }
        }
        else if (avoidStage == 2)
        {
            if (CanMove())
            {
                avoidExitTime -= Time.deltaTime; //keep ticking down while moving out of obstacles way
                enemy.pMover.cControl.SimpleMove(
                    enemy.tf.forward * enemy.forwardSpeed);
                if (avoidExitTime <= 0)
                {
                    avoidStage = 0; //if we can move and have turned long enough, leave avoidance
                }
            }
            else
            {
                avoidStage = 1; //go back to turning if still cant move
            }
        }
    }

    public bool CanMove()
    {
        RaycastHit hit; //stores collider information from raycast
        if (Physics.Raycast(enemy.tf.position, enemy.tf.forward, out hit,
            enemy.forwardSpeed)) //raycast to see what is infront
        {
            if (!hit.collider.CompareTag("Player")) //if the collider hit isn't the player, its an obstacle, cant move
            {
                return false;
            }

        }

        return true; //can move, nothing hit or player hit
    }
}
